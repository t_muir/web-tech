#HTTP Server file
# Original author: Sergio Tanzilli
# Source: https://www.acmesystems.it/python_http

# Modified for Suggestion Service and Python3
# Author: Thomas Muir, Department of Computer Science, Durham University

"""Webserver
    runs a local webserver accessable from http://localhost:8080

    the index gives a user friendly interface for interacting with the
    recommender class and the database, allowing users to rate films
    and retrieve their recommendations

    Provided endpoints:
    GET:
        - /recommend/*uid*: returns the recommended films for a given user id
        - /rated/*uid*: returns the previously rated films for a given user id
        - /genres/*uid*: returns the top genres for a given user id
        - /get-new-id: returns the next user id available for a new user
        - /get-movies-to-rate: gets a random set of unrated movies for a user to rate
    POST:
        - /update-rating: takes a form including uid, movieId and ratings to update the database
"""

from http.server import BaseHTTPRequestHandler, HTTPServer
from os import curdir, sep
import cgi
import recommender
import json
import pandas
import posterinterface as pi

PORT_NUMBER = 8080

RECOMMENDER = recommender.Recommender()
PG = pi.PosterGetter()

# This class will handles any incoming request from
# the browser
class myHandler(BaseHTTPRequestHandler):

    # Handler for the GET requests
    def do_GET(self):
        if self.path == "/":
            self.path = "frontend/index.html"

        try:
            # Check the file extension required and
            # set the right mime type
            sendReply = False
            if self.path.endswith(".html"):
                mimetype = 'text/html'
                sendReply = True
            if self.path.endswith(".jpg"):
                mimetype = 'image/jpg'
                sendReply = True
            if self.path.endswith(".gif"):
                mimetype = 'image/gif'
                sendReply = True
            if self.path.endswith(".js"):
                mimetype = 'application/javascript'
                sendReply = True
            if self.path.endswith(".css"):
                mimetype = 'text/css'
                sendReply = True
            if self.path.endswith(".json"):
                mimetype = 'text/json'
                sendReply = True

            if "/recommend" in self.path:
                uid = self.path[self.path.index('recommend/')+len('recommend/'):]
                try:
                    uid = int(uid)
                    try:
                        print('Getting user data...')
                        jsn = RECOMMENDER.recommendForUser(int(uid)).to_json()
                        jsn = PG.addUrls(jsn)
                        mimetype = 'text/html'
                        self.send_response(200)
                        self.send_header('Content-type', mimetype)
                        self.end_headers()
                        self.wfile.write(bytes(jsn, 'utf8'))
                    except IndexError:
                        mimetype = 'text/json'
                        self.send_response(200)
                        self.send_header('Content-type', mimetype)
                        self.end_headers()
                        self.wfile.write(bytes('Error: user does not exist', 'utf8'))

                except ValueError:
                    mimetype = 'text/json'
                    self.send_response(200)
                    self.send_header('Content-type', mimetype)
                    self.end_headers()
                    self.wfile.write(bytes('Error: invalid parsing userid', 'utf8'))
                return

            if "/rated" in self.path:
                uid = self.path[self.path.index('rated/')+len('rated/'):]
                # try:
                uid = int(uid)
                print(uid)
                try:
                    jsn = RECOMMENDER.getUserTopRated(int(uid)).to_json()
                    jsn = PG.addUrls(jsn)
                    mimetype = 'text/html'
                    self.send_response(200)
                    self.send_header('Content-type', mimetype)
                    self.end_headers()
                    self.wfile.write(bytes(jsn, 'utf8'))
                except IndexError:
                    mimetype = 'text/json'
                    self.send_response(200)
                    self.send_header('Content-type', mimetype)
                    self.end_headers()
                    self.wfile.write(bytes('Error: user does not exist', 'utf8'))

                # except ValueError:
                #     print(ValueError)
                #     mimetype = 'text/json'
                #     self.send_response(200)
                #     self.send_header('Content-type', mimetype)
                #     self.end_headers()
                #     self.wfile.write(bytes('Error: invalid parsing userid', 'utf8'))
                return

            if "/genres" in self.path:
                uid = self.path[self.path.index('genres/')+len('genres/'):]
                try:
                    uid = int(uid)
                    try:
                        jsn = json.dumps(RECOMMENDER.getUserGenres(int(uid)))
                        mimetype = 'text/html'
                        self.send_response(200)
                        self.send_header('Content-type', mimetype)
                        self.end_headers()
                        self.wfile.write(bytes(jsn, 'utf8'))
                    except IndexError:
                        mimetype = 'text/json'
                        self.send_response(200)
                        self.send_header('Content-type', mimetype)
                        self.end_headers()
                        self.wfile.write(bytes('Error: user does not exist', 'utf8'))

                except ValueError:
                    mimetype = 'text/json'
                    self.send_response(200)
                    self.send_header('Content-type', mimetype)
                    self.end_headers()
                    self.wfile.write(bytes('Error: invalid parsing userid', 'utf8'))
                return

            if "/get-new-id" in self.path:
                new_uid = RECOMMENDER.db.getLastUserID() + 1
                print(new_uid)
                response = {}
                response['uid'] = str(new_uid)
                jsn = json.dumps(response)
                mimetype = 'text/json'
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                self.wfile.write(bytes(jsn, 'utf8'))

            if "/get-movies-to-rate" in self.path:
                uid = self.path[self.path.index('get-movies-to-rate/')+len('get-movies-to-rate/'):]
                try:
                    uid = int(uid)
                    try:
                        jsn = RECOMMENDER.db.getRandomMoviesForRating(int(uid)).to_json()
                        jsn = PG.addUrls(jsn)
                        mimetype = 'text/html'
                        self.send_response(200)
                        self.send_header('Content-type', mimetype)
                        self.end_headers()
                        self.wfile.write(bytes(jsn, 'utf8'))
                    except IndexError:
                        mimetype = 'text/json'
                        self.send_response(200)
                        self.send_header('Content-type', mimetype)
                        self.end_headers()
                        self.wfile.write(bytes('Error: user does not exist', 'utf8'))

                except ValueError:
                    mimetype = 'text/json'
                    self.send_response(200)
                    self.send_header('Content-type', mimetype)
                    self.end_headers()
                    self.wfile.write(bytes('Error: invalid parsing userid', 'utf8'))
                return

            if sendReply == True:
                # Open the static file requested and send it
                f = open(curdir + sep + self.path)
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                self.wfile.write(bytes(f.read().replace('\n', ''), "utf8"))
                f.close()
            return

        except IOError:
            self.send_error(404, 'File Not Found: %s' % self.path)

    # Handler for the POST requests
    def do_POST(self):
        if self.path == "/update-rating":
            form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={'REQUEST_METHOD': 'POST',
                         'CONTENT_TYPE': self.headers['Content-Type'],
                         })
            RECOMMENDER.resetCurrent()
            userID = int(form['userId'].value)
            movieID = int(form['movieId'].value)
            rating = float(form['rating'].value)

            RECOMMENDER.db.updateDatabase(userID, movieID, rating)
            self.send_response(200)
            self.end_headers()
            self.wfile.write(bytes('User entry Updated', 'utf8'))
            return


try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print('Started Service on port ' + str(PORT_NUMBER))

    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print('Shutting down the web server')
    server.socket.close()
