from pathlib import Path
import urllib.parse
import urllib.request
import json

"""Poster Interface class
    provides access to local database of poster urls; if a poster is not available
    from the database it will request from the OMDB API to retrieve the information
    and will add to the database for future use
"""
class PosterGetter():
    def __init__(self):
        db_file = Path("database/posters.json")
        self.poster_db = {}
        if db_file.is_file():
            with open('database/posters.json', 'r') as f:
                self.poster_db = json.load(f)
        else:
            self.poster_db = {}

    def fetchMoviePoster(self, movieId, title, year):
        title_string = title.replace(' ', '+')

        request_data = {'apikey':'23b5b783', 't': title, 'y':str(year)}

        request_string = urllib.parse.urlencode(request_data)

        scheme, netloc, path, query, fragment = urllib.parse.urlsplit("http://www.omdbapi.com/?"+request_string)
        path = urllib.parse.quote(path)
        link = urllib.parse.urlunsplit((scheme, netloc, path, query, fragment))

        print('Requesting poster for: ' + link)
        response = urllib.request.urlopen(link).read()

        data = json.loads(response.decode('utf-8'))

        if 'Poster' in data:
            print('Poster retrieved!')
            url = data['Poster']

            self.poster_db[movieId] = url

            with open('database/posters.json', 'w') as f:
                json.dump(self.poster_db, f)
        else:
            url = ''

        return url

    def getMoviePoster(self, movieId, title, year):
        if str(movieId) not in self.poster_db:
            url = self.fetchMoviePoster(movieId, title, year)
        else:
            url = self.poster_db[str(movieId)]
        return url

    def addUrls(self, jsn):
        data = json.loads(jsn)
        url = {}

        for i in data['movieId']:
            movie_Id = data['movieId'][i]
            title_full = data['title'][i]
            title = title_full[:-6]
            year = title_full[-6:].replace('(', '')
            year = year.replace(')', '')

            if 'The' in title[-4:]:
                title = 'The ' + title[:-5]

            url[i] = self.getMoviePoster(movie_Id, title, year)

        data['urls'] = url

        jsn = json.dumps(data)

        return jsn
