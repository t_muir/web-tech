import database as db
import numpy as np
import pandas as pd
from scipy.sparse.linalg import svds

"""Recomender class
    provides interface and functions to access the databases and user information

    When a request is made, certain information is 'cached' about the user
    incase the same user makes the same or similar requests to prevent
    expensive recalculation
"""

class Recommender():

    def __init__(self):
        self.db = db.Database()
        self.currentUser = -1
        self.cuRatings = []
        self.cuUnratedPredictions = []

    def resetCurrent(self):
        self.currentUser = -1
        self.cuRatings = []
        self.cuUnratedPredictions = []

    # Returns the top recommended movies for a given user
    def recommendForUser(self, userID, num_recommendations=5):
        if userID != self.currentUser:
            # Recommend function first calculated the predicted rating score
            # for all of the unrated movies
            predictions_df = self.db.getPredictionsDataframe()
            movies_df = self.db.movies_data
            original_ratings_df = self.db.getUserMovieRating().fillna(-1)

            # Get and sort the user's predictions
            user_row_number = userID - 1  # UserID starts at 1, not 0
            sorted_user_predictions = pd.DataFrame(predictions_df.iloc[user_row_number].sort_values(
                ascending=False))
            sorted_user_predictions.columns = ['pred_rating']

            # Get the user's data and merge in the movie information.
            user_data = pd.DataFrame(original_ratings_df.iloc[user_row_number])
            user_data.columns = ['rating']
            cut_user_data = user_data.loc[user_data['rating'] != -1]

            user_full = (user_data.merge(movies_df, how='left', left_on='movieId', right_on='movieId').
                         sort_values(['rating'], ascending=False)
                         )

            # Recommend the highest predicted rating movies that the user hasn't seen yet.
            unrated = user_full.loc[user_full['rating'] == -1]

            unrated_predicted = unrated.merge(sorted_user_predictions, left_on='movieId',
                                              right_on='movieId', how='inner').sort_values(by=['pred_rating'], ascending=False)

            users_ratings = user_full.loc[user_full['rating'] != -1]

            self.currentUser = userID
            self.cuRatings = users_ratings
            self.cuUnratedPredictions = unrated_predicted

            # The function then sorts the top 20 by their fit to the users
            # genre preference
            genres = self.getUserGenres(userID)

            return unrated_predicted.head(num_recommendations)
        else:
            print('User persists...')
            return self.cuUnratedPredictions.head(num_recommendations)

    # Returns the top rated movies by a user
    def getUserTopRated(self, userID, num_ratings=5):
        if userID == self.currentUser:
            return self.cuRatings.head(num_ratings)
        else:
            users_ratings = self.db.getUserRated(userID)
            self.cuRatings = users_ratings

            return users_ratings.head(num_ratings)

    # Returns the top genres of the top ~20 rated movies of a user
    def getUserGenres(self, userID):
        users_ratings = self.getUserTopRated(userID, 20)

        genres = {}
        all_movie_genres = users_ratings['genres'].tolist()

        for i in all_movie_genres:
            current = i.split("|")
            for j in current:
                if j in genres:
                    genres[j] += 1
                else:
                    genres[j] = 1
        return sorted(genres, key=genres.__getitem__, reverse=True)[:5]

# Returns movies similar to a given movie
def correlationBetweenMovieSorted(database, movieId):
    user_ratings = database.getUserMovieRating()
    movie_ratings = user_ratings[movieId]

    movie_similarities = user_ratings.corrwith(movie_ratings)

    movie_correlations = pd.DataFrame(
        movie_similarities, columns=['Correlation'])
    movie_correlations.dropna(inplace=True)

    return movie_correlations.sort_values('Correlation', ascending=False)
