import pandas as pd
import time
import numpy as np
from scipy.sparse.linalg import svds

"""Database class
    provides interface to access and modification of the CSV files which contain
    the database information also provides some database operation functions to
    allow for easy access to common merges and transforms
"""
class Database():
    def __init__(self):
        self.movies_data = pd.read_csv("database/movies.csv")
        self.ratings_data = pd.read_csv("database/ratings.csv")

    def getPredictionsDataframe(self):
        user_ratings = self.getUserMovieRating().fillna(-1)

        R = user_ratings.as_matrix()

        user_ratings_mean = np.mean(R, axis=1)

        R_demeaned = R - user_ratings_mean.reshape(-1, 1)

        U, sigma, Vt = svds(R_demeaned, k=50)

        sigma = np.diag(sigma)

        all_user_predicted_ratings = np.dot(
            np.dot(U, sigma), Vt) + user_ratings_mean.reshape(-1, 1)
        preds_df = pd.DataFrame(all_user_predicted_ratings,
                                columns=user_ratings.columns)

        return preds_df

    def getRatingAndMovies(self):
        return pd.merge(self.ratings_data, self.movies_data, on='movieId')

    def getMeanMovieRatings(self):
        return self.getRatingAndMovies().groupby('title')['rating'].mean()

    def getMeanMovieRatingsSorted(self):
        return self.getRatingAndMovies().groupby('title')['rating'].mean().sort_values(ascending=False)

    def getMovieRatingCountsSorted(self):
        return self.getRatingAndMovies().groupby('title')['rating'].count().sort_values(ascending=False).head()

    def getUserMovieRating(self):
        return self.getRatingAndMovies().pivot_table(index='userId', columns='movieId', values='rating')

    def getLastUserID(self):
        return self.ratings_data['userId'].max()

    def getUserRated(self, userID):
        movies_df = self.movies_data
        original_ratings_df = self.getUserMovieRating().fillna(-1)
        user_row_number = userID - 1  # UserID starts at 1, not 0

        user_data = pd.DataFrame(original_ratings_df.iloc[user_row_number])
        user_data.columns = ['rating']
        cut_user_data = user_data.loc[user_data['rating'] != -1]

        user_full = (user_data.merge(movies_df, how='left', left_on='movieId', right_on='movieId').
                         sort_values(['rating'], ascending=False)
                         )

        users_ratings = user_full.loc[user_full['rating'] != -1]

        return users_ratings

    def getUnratedMovies(self, uid):
        users_ratings = self.getUserRated(uid)
        movie_ids = users_ratings['movieId']

        unrated = self.movies_data[~self.movies_data['movieId'].isin(movie_ids)]

        return unrated

    def updateDatabase(self, uID, mID, r):
        timestamp = time.time()
        index = self.ratings_data.index[(self.ratings_data.movieId == mID) & (self.ratings_data.userId == uID)]

        if len(index.tolist()) == 0:
            new_df = pd.DataFrame([[uID, mID, r, timestamp]], columns=['userId', 'movieId', 'rating', 'timestamp'])
            self.ratings_data = self.ratings_data.append(new_df, ignore_index=True)
        else:
            self.ratings_data.loc[(self.ratings_data.movieId == mID) & (self.ratings_data.userId == uID), 'rating'] = r
            self.ratings_data.loc[(self.ratings_data.movieId == mID) & (self.ratings_data.userId == uID), 'timestamp'] = timestamp

        self.ratings_data.to_csv("database/ratings.csv")

    def getRandomMoviesForRating(self, uid, number_of_movies=5):
        try:
            unrated = self.getUnratedMovies(uid)
        except IndexError:
            unrated = self.movies_data;
        return unrated.sample(n=number_of_movies)
