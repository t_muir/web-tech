let langs = {
  FR: "/frontend/lang_fr.json",
  EN: "/frontend/lang_en.json"
};

let titles = {
  HEADING: $('#title'),
  RECHEAD: $('#recommendedTitle'),
  PREVHEAD: $('#previousTitle'),
  NEWUSR: $('#newuser'),
  ENTUSER: $('#enteruser'),
  SUBMIT: $('#idsubmit'),
  TGENRE: $('#genresTitle'),
  SHOWCRE: $('#showcreate'),
  DIFFMOVIE: $('#refreshToRate'),
  NEWUSR: $('#newuser'),
  GETSTRT: $('#getStarted')
};

let currentLang = 'EN';

let userID = -1;

$('#idsubmit').click(function() {
  let uid = $( "#uid" ).val();

  setCookie("uid", uid, 2);

  $('div.previouslyrated').text('');
  $('div.recommended').text('');
  $('div.topgenres').text('');
  $('#previousTitle').addClass('hidden');
  $('#recommendedTitle').addClass('hidden');
  $('#createNewUser').addClass('hidden');
  $('#genresTitle').addClass('hidden');

  getRecomendations(uid);
  getPreviouslyRated(uid);
  getTopGenres(uid);
});

$('.translate').click(function() {
  setCookie("lang", $(this).data('lang'), 2);
  translate(langs[$(this).data('lang')]);
});

$('#showcreate').click(function() {
  $('#createNewUser').removeClass('hidden');
  $('div.previouslyrated').text('');
  $('div.recommended').text('');
  $('div.firstRatings').text('');
  $('div.topgenres').text('');
  $('#previousTitle').addClass('hidden');
  $('#recommendedTitle').addClass('hidden');
  $('#genresTitle').addClass('hidden');

  let url = "http://localhost:8080/get-new-id";
  $.ajax({
    url: url,
    context: document.body
  }).done(function(response) {
    $('body').css("background-image", "none");
    userID = response['uid'];
    $( "#uid" ).val(userID);
    $('#newuser').after(" - " + userID);
    getToRate(userID);
  });
});

function getToRate(uid){
  $('body').css("background-image", "url('https://i.redd.it/ounq1mw5kdxy.gif')");
  $('div.firstRatings').text('');
  let url = "http://localhost:8080/get-movies-to-rate/" + uid;
  $.ajax({
    url: url,
    context: document.body
  }).done(function(response) {
    $('body').css("background-image", "none");
    let data = $.parseJSON(response);
    console.log(data);
    $.each(data['title'], function(i, val) {
      let imgsrc = 'https://i.imgur.com/Z2MYNbj.png/large_movie_poster.png';
      if(data['urls'][i] != ''){
        imgsrc = data['urls'][i]
      }
      $('div.firstRatings').prepend('<div class="col-sm">'+
      '<img class="poster" src="'+ imgsrc +'">'+
      '<h3>' + val + '</h3>'+
      '<div id="rateYo'+i+'"></div></div>');
      $("#rateYo"+i).rateYo({
        rating: 0,
        starWidth: "23px",
        onSet: function (given, rateYoInstance) {
          updateRatingNoRefresh(uid, data['movieId'][i], given)
        }
      });
    });
  });
}

$('#refreshToRate').click(function() {
  getToRate(userID);
});

function getTopGenres(uid){
  $('body').css("background-image", "url('https://i.redd.it/ounq1mw5kdxy.gif')");
  let url = "http://localhost:8080/genres/" + uid;
  $.ajax({
    url: url,
    context: document.body
  }).done(function(response) {
    $('body').css("background-image", "none");
    console.log(response);
    displayGenres(response, uid);
  });
}

function getPreviouslyRated(uid){
  $('body').css("background-image", "url('https://i.redd.it/ounq1mw5kdxy.gif')");
  let url = "http://localhost:8080/rated/" + uid;
  $.ajax({
    url: url,
    context: document.body
  }).done(function(response) {
    $('body').css("background-image", "none");
    displayRated(response, uid);
  });
}

function getRecomendations(uid){
  $('body').css("background-image", "url('https://i.redd.it/ounq1mw5kdxy.gif')");
  let url = "http://localhost:8080/recommend/" + uid;
  $.ajax({
    url: url,
    context: document.body
  }).done(function(response) {
    $('body').css("background-image", "none");
    displayRecomendations(response, uid);
  });
}

function displayRated(response, uid){
  let data = $.parseJSON(response);
  $('#previousTitle').removeClass('hidden');
  $.each(data['title'], function(i, val) {
    let rating = data['rating'][i];
    let imgsrc = 'https://i.imgur.com/Z2MYNbj.png/large_movie_poster.png';
    if(data['urls'][i] != ''){
      imgsrc = data['urls'][i]
    }
    $('div.previouslyrated').prepend('<div class="col-sm">'+
    '<img class="poster" src="'+ imgsrc +'">'+
    '<h3>' + val + '</h3>'+
    '<div id="rateYo'+i+'"></div></div>');
    $("#rateYo"+i).rateYo({
      rating: rating,
      starWidth: "23px",
      onSet: function (given, rateYoInstance) {
        updateRating(uid, data['movieId'][i], given)
      }
    });
  });
}

function displayRecomendations(response, uid){
  let data = $.parseJSON(response);
  $('#recommendedTitle').removeClass('hidden');
  $.each(data['title'], function(i, val) {
    let imgsrc = 'https://i.imgur.com/Z2MYNbj.png/large_movie_poster.png';
    if(data['urls'][i] != ''){
      imgsrc = data['urls'][i]
    }

    $('div.recommended').prepend('<div class="col-sm">'+
    '<img class="poster" src="'+imgsrc+'">'+
    '<h3>' + val + '</h3>'+
    '<div id="rateYo'+i+'"></div></div>');
    $("#rateYo"+i).rateYo({
      rating: 0,
      starWidth: "23px",
      onSet: function (given, rateYoInstance) {
        updateRating(uid, data['movieId'][i], given)
      }
    });
  });
}

function displayGenres(response, uid){
  let data = $.parseJSON(response);
  $('#genresTitle').removeClass('hidden');
  $.each(data, function(i, val) {
    $('div.topgenres').prepend('<div class="col-sm">'+
    '<h3>' + val + '</h3>');
  });
}

function updateRating(uid, movieId, rating){
  data = {
    userId: uid,
    movieId: movieId,
    rating: rating
  };

  $.ajax({
    type: "POST",
    url: 'http://localhost:8080/update-rating',
    context: document.body,
    data: data
  }).done(function(response) {
    console.log(response);

    $('div.previouslyrated').text('');
    $('div.recommended').text('');
    $('#previousTitle').addClass('hidden');
    $('div.topgenres').text('');
    $('#recommendedTitle').addClass('hidden');
    $('#genresTitle').addClass('hidden');

    getPreviouslyRated(uid);
    getRecomendations(uid);
    getTopGenres(uid);
  });
}

function updateRatingNoRefresh(uid, movieId, rating){
  data = {
    userId: uid,
    movieId: movieId,
    rating: rating
  };

  $.ajax({
    type: "POST",
    url: 'http://localhost:8080/update-rating',
    context: document.body,
    data: data
  }).done(function(response) {

  });
}

function translate(lang){
  $.getJSON(lang, function(data) {
      $.each(titles, function(i, val) {
        val.text(data[i]);
        val.attr('value', data[i]);
      });
  });
}

/* Following code adaptation of w3schools cookie:
    https://www.w3schools.com/js/js_cookies.asp */
function checkLangCookie() {
  var lang = getCookie("lang");
  if (lang != 'EN') {
   translate(langs[lang]);
   currentLang = lang;
  }
}

function checkUIDCookie() {
  var id = getCookie("uid");
  if (id) {
    $( "#uid" ).val(id);
    $('#idsubmit').trigger( "click" );
  }
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

$( document ).ready(function() {
    checkLangCookie();
    checkUIDCookie();
});
